<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "scheduled_visits".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $name
 * @property string $email
 * @property integer $mobile
 * @property string $status
 * @property string $scheduled_time
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 */
class ScheduledVisits extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scheduled_visits';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'name', 'email', 'mobile','scheduled_time','occupancy'], 'required'],
            [['mub_user_id', 'property_id','mobile'], 'integer'],
            [['email'],'email'],
            ['mobile','number'],
            ['mobile','string', 'max' => 13 , 'min' => 10],
            [['status', 'del_status','occupancy'], 'string'],
            [['scheduled_time', 'created_at', 'updated_at'], 'safe'],
            [['name', 'email'], 'string', 'max' => 50],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'property_id' => 'Property ID',
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'occupancy' => 'Occupancy',
            'status' => 'Status',
            'scheduled_time' => 'Scheduled Time',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
