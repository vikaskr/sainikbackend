<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $resturant_name
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property string $booking_id
 * @property string $address
 * @property string $alt_address
 * @property string $item
 * @property integer $amount
 * @property string $payment_mode
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 */
class Booking extends \yii\db\ActiveRecord
{
    // public function behaviors()
    // {
    //     return [
    //         [
    //             'class' => TimestampBehavior::className(),
    //             'attributes' => [
    //                 ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
    //             ],
    //             'value' => new Expression('NOW()'),
    //         ],
    //     ];
    // }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'amount'], 'integer'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['resturant_name', 'name', 'email', 'mobile','quantity'], 'string', 'max' => 50],
            [['booking_id', 'address', 'alt_address', 'item', 'payment_mode','lat','long','street_address'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'resturant_name' => 'Resturant Name',
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'booking_id' => 'Booking ID',
            'address' => 'Address',
            'alt_address' => 'Alt Address',
            'lat' => 'Lat',
            'long' => 'Long',
            'street_address' => 'Street Address',
            'item' => 'Item',
            'quantity' => 'quantity',
            'amount' => 'Amount',
            'payment_mode' => 'Payment Mode',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
