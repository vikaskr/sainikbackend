<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mub_user_images".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $title
 * @property string $description
 * @property string $url
 * @property string $thumbnail_url
 * @property string $thumbnail_path
 * @property string $full_path
 * @property string $visible
 * @property string $keyword
 * @property integer $width
 * @property integer $height
 * @property integer $display_width
 * @property integer $display_hieght
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property AlbumImages[] $albumImages
 * @property MubUser $mubUser
 * @property PageImages[] $pageImages
 */
class MubUserImages extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mub_user_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'width', 'height', 'display_width', 'display_hieght'], 'integer'],
            [['title', 'description', 'url', 'thumbnail_url', 'full_path'], 'required'],
            [['visible', 'type', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 50],
            [['description', 'url', 'thumbnail_url', 'thumbnail_path', 'full_path'], 'string', 'max' => 100],
            [['keyword'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mub_user_id' => Yii::t('app', 'Mub User ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'url' => Yii::t('app', 'Url'),
            'thumbnail_url' => Yii::t('app', 'Thumbnail Url'),
            'thumbnail_path' => Yii::t('app', 'Thumbnail Path'),
            'full_path' => Yii::t('app', 'Full Path'),
            'visible' => Yii::t('app', 'Visible'),
            'keyword' => Yii::t('app', 'Keyword'),
            'width' => Yii::t('app', 'Width'),
            'height' => Yii::t('app', 'Height'),
            'display_width' => Yii::t('app', 'Display Width'),
            'display_hieght' => Yii::t('app', 'Display Hieght'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbumImages()
    {
        return $this->hasMany(AlbumImages::className(), ['image_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageImages()
    {
        return $this->hasMany(PageImages::className(), ['image_id' => 'id'])->where(['del_status' => '0']);
    }
}
