<?php 
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url; 
?>
<style type="text/css">
.help-block-error {
    color: red!important;
}
.modal-header {
    padding: 15px;
    border-bottom: 1px solid #fff;}

</style>
<div class="modal-header">
    <div class="row container-fluid">
        <div class="pull-left" style=" margin-left: 2.9em;">Don't have an account ? <a  href="/site/client" class="signup-user" style="cursor: pointer; font-size: 18px; color: red;">Create New Account.</a></div>
        <div class="pull-right"><button style="display:inline" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    </div>                        
</div>
<div class="modal-body real-spa">
<div class="login-grids">
    <div class="login">
        <div class="login-right">
        <?php $form = ActiveForm::begin(['layout' => 'horizontal','options' => ['id' => 'frontend-signin','method' => 'POST','data-pjax' => true],'action' => ['/']]); ?>
               <div class="col-md-12 text-center" style="margin-bottom: 1em; margin-top: -1em"><h2>Login</h2></div>
               <div class="row">
                <div class="col-md-12">
                <?= $form->field($model, 'username')->textInput(['class' => 'form-control']);?><br>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'password')->passwordInput();?><br>
            </div>
                </div>
                <div class="row col-md-12">
                    <div class="row col-md-4 col-xs-5 marg" style="margin-left: 9em;">
                        <?= $form->field($model, 'rememberMe')->checkbox([
                      'template' => "<div class=\"single-bottom\">{input} {label}</div>\n<div class=\"col-lg-4\">{error}</div>"]) ?>
                  </div>  
                <div class="col-md-5 col-xs-5"><p class="frontend-forget" id="forget" style="cursor: pointer; text-decoration: underline;"> Forgot Password</p></div>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-3">
                <input type="submit" value="Login" style="float: none;width: 150px; background: #058a0b; padding: 5px; color: #fff;">
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
   
<br><br><br><br><br><br>
    <p style="color: #000!important;">By logging in you agree to our <a style="color: #6298c7!important;" target="_blank">Terms and Conditions</a> and <a style="color: #6298c7!important;" target="_blank">Privacy Policy</a></p>
</div>
</div>
<div class="modal-footer">  
</div>
