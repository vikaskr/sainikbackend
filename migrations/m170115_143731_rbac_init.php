<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\migrations;

Use Yii;

use \yii\base\InvalidConfigException;
use \yii\rbac\DbManager;

/**
 * Initializes RBAC tables
 *
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @since 2.0
 */
class m170115_143731_rbac_init extends \yii\db\Migration
{
    /**
     * @throws yii\base\InvalidConfigException
     * @return DbManager
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
        return $authManager;
    }

    /**
     * @return bool
     */
    protected function isMSSQL()
    {
        return $this->db->driverName === 'mssql' || $this->db->driverName === 'sqlsrv' || $this->db->driverName === 'dblib';
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($authManager->ruleTable, [
            'name' => $this->string(64)->notNull(),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'PRIMARY KEY (name)',
        ], $tableOptions);

        $this->createTable($authManager->itemTable, [
            'name' => $this->string(64)->notNull(),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
            'rule_name' => $this->string(64),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'PRIMARY KEY (name)',
            'FOREIGN KEY (rule_name) REFERENCES ' . $authManager->ruleTable . ' (name)'.
                ($this->isMSSQL() ? '' : ' ON DELETE SET NULL ON UPDATE CASCADE'),
        ], $tableOptions);
        $this->createIndex('idx-auth_item-type', $authManager->itemTable, 'type');

        $this->createTable($authManager->itemChildTable, [
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
            'PRIMARY KEY (parent, child)',
            'FOREIGN KEY (parent) REFERENCES ' . $authManager->itemTable . ' (name)'.
                ($this->isMSSQL() ? '' : ' ON DELETE CASCADE ON UPDATE CASCADE'),
            'FOREIGN KEY (child) REFERENCES ' . $authManager->itemTable . ' (name)'.
                ($this->isMSSQL() ? '' : ' ON DELETE CASCADE ON UPDATE CASCADE'),
        ], $tableOptions);

        $this->createTable($authManager->assignmentTable, [
            'item_name' => $this->string(64)->notNull(),
            'user_id' => $this->string(64)->notNull(),
            'created_at' => $this->integer(),
            'PRIMARY KEY (item_name, user_id)',
            'FOREIGN KEY (item_name) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->createPermission();
        $this->createRole();
        $this->assignRole();
        $this->createDashboardRule();
        
        if($this->isMsSQL()) {
            $this->execute("CREATE TRIGGER dbo.trigger_auth_item_child
            ON dbo.{$authManager->itemTable}
            INSTEAD OF DELETE, UPDATE
            AS
            DECLARE @old_name VARCHAR (64) = (SELECT name FROM deleted)
            DECLARE @new_name VARCHAR (64) = (SELECT name FROM inserted)
            BEGIN
            IF COLUMNS_UPDATED() > 0
                BEGIN
                    IF @old_name <> @new_name
                    BEGIN
                        ALTER TABLE auth_item_child NOCHECK CONSTRAINT FK__auth_item__child;
                        UPDATE auth_item_child SET child = @new_name WHERE child = @old_name;
                    END
                UPDATE auth_item
                SET name = (SELECT name FROM inserted),
                type = (SELECT type FROM inserted),
                description = (SELECT description FROM inserted),
                rule_name = (SELECT rule_name FROM inserted),
                data = (SELECT data FROM inserted),
                created_at = (SELECT created_at FROM inserted),
                updated_at = (SELECT updated_at FROM inserted)
                WHERE name IN (SELECT name FROM deleted)
                IF @old_name <> @new_name
                    BEGIN
                        ALTER TABLE auth_item_child CHECK CONSTRAINT FK__auth_item__child;
                    END
                END
                ELSE
                    BEGIN
                        DELETE FROM dbo.{$authManager->itemChildTable} WHERE parent IN (SELECT name FROM deleted) OR child IN (SELECT name FROM deleted);
                        DELETE FROM dbo.{$authManager->itemTable} WHERE name IN (SELECT name FROM deleted);
                    END
            END;");
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        if($this->isMsSQL()) {
            $this->execute('DROP dbo.trigger_auth_item_child;');
        }

        $this->dropTable($authManager->assignmentTable);
        $this->dropTable($authManager->itemChildTable);
        $this->dropTable($authManager->itemTable);
        $this->dropTable($authManager->ruleTable);
    }
    /**
    * create permission
    * @author Praveen Kumar <praveen@makeubig.com>
    */
    protected function createPermission() 
    {
        $auth = Yii::$app->authManager;

        /**
         *              DASHBOARD PERMISSIONS
         */
        $indexd = $auth->createPermission('dashboard/index');
        $indexd->description = 'index dashboard';
        $auth->add($indexd);

        $created = $auth->createPermission('dashboard/create');
        $created->description = 'create dashboard';
        $auth->add($created);
        
        $updated = $auth->createPermission('dashboard/update');
        $updated->description = 'update dashboard';
        $auth->add($updated);
        
        $viewd = $auth->createPermission('dashboard/view');
        $viewd->description = 'view dashboard';
        $auth->add($viewd);
        
        $deleted = $auth->createPermission('dashboard/delete');
        $deleted->description = 'delete dashboard';
        $auth->add($deleted);

         /**
         *              Restaurant PERMISSIONS
         */
        $indexp = $auth->createPermission('restaurant/index');
        $indexp->description = 'index Restaurant';
        $auth->add($indexp);

        $createp = $auth->createPermission('restaurant/create');
        $createp->description = 'create Restaurant';
        $auth->add($createp);
        
        $updatep = $auth->createPermission('restaurant/update');
        $updatep->description = 'update Restaurant';
        $auth->add($updatep);
        
        $viewp = $auth->createPermission('restaurant/view');
        $viewp->description = 'view Restaurant';
        $auth->add($viewp);
        
        $deleteg = $auth->createPermission('restaurant/delete');
        $deleteg->description = 'delete Restaurant';
        $auth->add($deleteg);

        /**
         *              Restaurant PERMISSIONS
         */
        $indexCategory = $auth->createPermission('category/index');
        $indexCategory->description = 'index Category';
        $auth->add($indexCategory);

        $createCategory = $auth->createPermission('category/create');
        $createCategory->description = 'create Category';
        $auth->add($createCategory);
        
        $updateCategory = $auth->createPermission('category/update');
        $updateCategory->description = 'update Category';
        $auth->add($updateCategory);
        
        $viewCategory = $auth->createPermission('category/view');
        $viewCategory->description = 'view Category';
        $auth->add($viewCategory);
        
        $deleteCategory = $auth->createPermission('category/delete');
        $deleteCategory->description = 'delete Category';
        $auth->add($deleteCategory);

        /*
                    Menu permission
        */ 
        $indexMenu = $auth->createPermission('menu/index');
        $indexMenu->description = 'index menu';
        $auth->add($indexMenu);

        $createMenu = $auth->createPermission('menu/create');
        $createMenu->description = 'create menu';
        $auth->add($createMenu);
        
        $updateMenu = $auth->createPermission('menu/update');
        $updateMenu->description = 'update menu';
        $auth->add($updateMenu);
        
        $viewMenu = $auth->createPermission('menu/view');
        $viewMenu->description = 'view menu';
        $auth->add($viewMenu);
        
        $deleteMenu = $auth->createPermission('menu/delete');
        $deleteMenu->description = 'delete menu';
        $auth->add($deleteMenu);

        /*
                    Offers permission
        */ 
        $indexOffers = $auth->createPermission('offers/index');
        $indexOffers->description = 'index offers';
        $auth->add($indexOffers);

        $createOffers = $auth->createPermission('offers/create');
        $createOffers->description = 'create offers';
        $auth->add($createOffers);
        
        $updateOffers = $auth->createPermission('offers/update');
        $updateOffers->description = 'update offers';
        $auth->add($updateOffers);
        
        $viewOffers = $auth->createPermission('offers/view');
        $viewOffers->description = 'view offers';
        $auth->add($viewOffers);
        
        $deleteOffers = $auth->createPermission('offers/delete');
        $deleteOffers->description = 'delete offers';
        $auth->add($deleteOffers);

                // Items Permissions

        $indexbe = $auth->createPermission('item/index');
        $indexbe->description = 'index Restaurant item';
        $auth->add($indexbe);

        $createbe = $auth->createPermission('item/create');
        $createbe->description = 'create Restaurant item';
        $auth->add($createbe);
        
        $updatebe = $auth->createPermission('item/update');
        $updatebe->description = 'update Restaurant item';
        $auth->add($updatebe);
        
        $viewbe = $auth->createPermission('item/view');
        $viewbe->description = 'view Restaurant item';
        $auth->add($viewbe);
        
        $deletebe = $auth->createPermission('item/delete');
        $deletebe->description = 'delete Restaurant item';
        $auth->add($deletebe);

        /*
                    Service permission
        */ 
        $indexService = $auth->createPermission('service/index');
        $indexService->description = 'index service';
        $auth->add($indexService);

        $createService = $auth->createPermission('service/create');
        $createService->description = 'create service';
        $auth->add($createService);
        
        $updateService = $auth->createPermission('service/update');
        $updateService->description = 'update service';
        $auth->add($updateService);
        
        $viewService = $auth->createPermission('service/view');
        $viewService->description = 'view service';
        $auth->add($viewService);
        
        $deleteService = $auth->createPermission('service/delete');
        $deleteService->description = 'delete service';
        $auth->add($deleteService);

        /*
                    report permission
        */ 
        $indexReport = $auth->createPermission('report/index');
        $indexReport->description = 'index report';
        $auth->add($indexReport);

        $createReport = $auth->createPermission('report/create');
        $createReport->description = 'create report';
        $auth->add($createReport);
        
        $updateReport = $auth->createPermission('report/update');
        $updateReport->description = 'update report';
        $auth->add($updateReport);
        
        $viewReport = $auth->createPermission('report/view');
        $viewReport->description = 'view report';
        $auth->add($viewReport);
        
        $deleteReport = $auth->createPermission('report/delete');
        $deleteReport->description = 'delete report';
        $auth->add($deleteReport);
        
        
        /**
         *              User PERMISSIONS
         */
        $indexu = $auth->createPermission('user/index');
        $indexu->description = 'index user';
        $auth->add($indexu);

        $createu = $auth->createPermission('user/create');
        $createu->description = 'create user';
        $auth->add($createu);
        
        $updateu = $auth->createPermission('user/update');
        $updateu->description = 'update user';
        $auth->add($updateu);
        
        $viewu = $auth->createPermission('user/view');
        $viewu->description = 'view user';
        $auth->add($viewu);
        
        $deleteu = $auth->createPermission('user/delete');
        $deleteu->description = 'delete user';
        $auth->add($deleteu);

        $indexa = $auth->createPermission('auth');
        $indexa->description = 'Auth Access';
        $auth->add($indexa);
    }

    /**
    * create role
    * @author Praveen Kumar <praveen@makeubig.com>
    */
    protected function createRole() 
    {
        $auth = Yii::$app->authManager;
        //admin -> Own view/update
        //superadmin -> (admin) and -> view/update/create/index/delete
        
        $indexd = $auth->createPermission('dashboard/index');
        $created = $auth->createPermission('dashboard/create');
        $updated = $auth->createPermission('dashboard/update');
        $viewd = $auth->createPermission('dashboard/view');
        $deleted = $auth->createPermission('dashboard/delete');
        
        $indexu = $auth->createPermission('user/index');
        $createu = $auth->createPermission('user/create');
        $updateu = $auth->createPermission('user/update');
        $viewu = $auth->createPermission('user/view');
        $deleteu = $auth->createPermission('user/delete');
        
        $indexp = $auth->createPermission('restaurant/index');
        $createp = $auth->createPermission('restaurant/create');
        $updatep = $auth->createPermission('restaurant/update');
        $viewp = $auth->createPermission('restaurant/view');
        $deletep = $auth->createPermission('restaurant/delete');

        $indexbe = $auth->createPermission('item/index');
        $createbe = $auth->createPermission('item/create');
        $updatebe = $auth->createPermission('item/update');
        $viewbe = $auth->createPermission('item/view');
        $deletebe = $auth->createPermission('item/delete');

        $indexService = $auth->createPermission('service/index');
        $createService = $auth->createPermission('service/create');
        $updateService  = $auth->createPermission('service/update');
        $deleteService = $auth->createPermission('service/delete');
        $viewService = $auth->createPermission('service/view');

        $indexMenu = $auth->createPermission('menu/index');
        $createMenu = $auth->createPermission('menu/create');
        $updateMenu  = $auth->createPermission('menu/update');
        $deleteMenu = $auth->createPermission('menu/delete');
        $viewMenu = $auth->createPermission('menu/view');

        $indexOffers = $auth->createPermission('offers/index');
        $createOffers = $auth->createPermission('offers/create');
        $updateOffers  = $auth->createPermission('offers/update');
        $deleteOffers = $auth->createPermission('offers/delete');
        $viewOffers = $auth->createPermission('offers/view');

        $indexCategory = $auth->createPermission('category/index');
        $createCategory = $auth->createPermission('category/create');
        $updateCategory = $auth->createPermission('category/update');
        $deleteCategory = $auth->createPermission('category/delete');
        $viewCategory = $auth->createPermission('category/view');


        $indexa = $auth->createPermission('auth');  

        $subadmin = $auth->createRole('subadmin');
        $auth->add($subadmin);
        $admin = $auth->createRole('admin');
        $client = $auth->createRole('client');
        $auth->add($admin);
        $auth->add($client);
        $auth->addChild($admin, $subadmin);
        $auth->addChild($admin, $indexa);
        $auth->addChild($admin, $client);
        
        $superadmin = $auth->createRole('mubadmin');
        $auth->add($superadmin);
        $auth->addChild($superadmin, $admin);
        $auth->addChild($superadmin, $indexd);
        $auth->addChild($superadmin, $created);
        $auth->addChild($superadmin, $deleted);
        $auth->addChild($superadmin, $updated);
        $auth->addChild($superadmin, $viewd);
        $auth->addChild($superadmin, $indexu);
        $auth->addChild($superadmin, $createu);
        $auth->addChild($superadmin, $deleteu);
        $auth->addChild($superadmin, $updateu);
        $auth->addChild($superadmin, $viewu);
        $auth->addChild($superadmin, $indexp);
        $auth->addChild($superadmin, $createp);
        $auth->addChild($superadmin, $deletep);
        $auth->addChild($superadmin, $updatep);
        $auth->addChild($superadmin, $viewp);
        $auth->addChild($superadmin, $indexbe);
        $auth->addChild($superadmin, $createbe);
        $auth->addChild($superadmin, $deletebe);
        $auth->addChild($superadmin, $updatebe);
        $auth->addChild($superadmin, $viewbe);
        $auth->addChild($superadmin, $indexService);
        $auth->addChild($superadmin, $createService);
        $auth->addChild($superadmin, $deleteService);
        $auth->addChild($superadmin, $updateService);
        $auth->addChild($superadmin, $viewService);
        $auth->addChild($superadmin, $indexMenu);
        $auth->addChild($superadmin, $createMenu);
        $auth->addChild($superadmin, $deleteMenu);
        $auth->addChild($superadmin, $updateMenu);
        $auth->addChild($superadmin, $viewMenu);
        $auth->addChild($superadmin, $indexOffers);
        $auth->addChild($superadmin, $createOffers);
        $auth->addChild($superadmin, $deleteOffers);
        $auth->addChild($superadmin, $updateOffers);
        $auth->addChild($superadmin, $viewOffers);
        $auth->addChild($superadmin, $indexCategory);
        $auth->addChild($superadmin, $createCategory);
        $auth->addChild($superadmin, $deleteCategory);
        $auth->addChild($superadmin, $updateCategory);
        $auth->addChild($superadmin, $viewCategory);
    
        $auth->addChild($admin, $indexd);
        $auth->addChild($admin, $created);
        $auth->addChild($admin, $deleted);
        $auth->addChild($admin, $updated);
        $auth->addChild($admin, $viewd);
        $auth->addChild($admin, $indexp);
        $auth->addChild($admin, $createp);
        $auth->addChild($admin, $deletep);
        $auth->addChild($admin, $updatep);
        $auth->addChild($admin, $viewp);
        $auth->addChild($admin, $indexbe);
        $auth->addChild($admin, $createbe);
        $auth->addChild($admin, $deletebe);
        $auth->addChild($admin, $updatebe);
        $auth->addChild($admin, $viewbe);
        $auth->addChild($admin, $indexu);
        $auth->addChild($admin, $createu);
        $auth->addChild($admin, $deleteu);
        $auth->addChild($admin, $updateu);
        $auth->addChild($admin, $viewu);
        $auth->addChild($admin, $indexService);
        $auth->addChild($admin, $createService);
        $auth->addChild($admin, $deleteService);
        $auth->addChild($admin, $updateService);
        $auth->addChild($admin, $viewService);
        $auth->addChild($admin, $indexMenu);
        $auth->addChild($admin, $createMenu);
        $auth->addChild($admin, $deleteMenu);
        $auth->addChild($admin, $updateMenu);
        $auth->addChild($admin, $viewMenu);
        $auth->addChild($admin, $indexOffers);
        $auth->addChild($admin, $createOffers);
        $auth->addChild($admin, $deleteOffers);
        $auth->addChild($admin, $updateOffers);
        $auth->addChild($admin, $viewOffers);
        $auth->addChild($admin, $indexCategory);
        $auth->addChild($admin, $createCategory);
        $auth->addChild($admin, $deleteCategory);
        $auth->addChild($admin, $updateCategory);
        $auth->addChild($admin, $viewCategory);

        $auth->addChild($subadmin, $indexd);
        $auth->addChild($subadmin, $created);
        $auth->addChild($subadmin, $deleted);
        $auth->addChild($subadmin, $updated);
        $auth->addChild($subadmin, $viewd);
        $auth->addChild($subadmin, $indexp);
        $auth->addChild($subadmin, $createp);
        $auth->addChild($subadmin, $updatep);
        $auth->addChild($subadmin, $viewp);
        $auth->addChild($subadmin, $indexbe);
        $auth->addChild($subadmin, $createbe);
        $auth->addChild($subadmin, $updatebe);
        $auth->addChild($subadmin, $viewbe);
        $auth->addChild($subadmin, $indexMenu);
        $auth->addChild($subadmin, $createMenu);
        $auth->addChild($subadmin, $deleteMenu);
        $auth->addChild($subadmin, $updateMenu);
        $auth->addChild($subadmin, $viewMenu);
        $auth->addChild($subadmin, $indexOffers);
        $auth->addChild($subadmin, $createOffers);
        $auth->addChild($subadmin, $deleteOffers);
        $auth->addChild($subadmin, $updateOffers);
        $auth->addChild($subadmin, $viewOffers);

    }
    /**
    * create user rule
    * @author Praveen Kumar <praveen@makeubig.com>
    */
    protected function createDashboardRule() 
    {
        $auth = Yii::$app->authManager;
        // add the rule
        $rule = new \app\components\rbac\AdminUserRule();
        $auth->add($rule);

        // add the "updateOwnUser" permission and associate the rule with it.
        $updateOwnUser = $auth->createPermission('updateOwnUser');
        $updateOwnUser->description = 'Update own User';
        $updateOwnUser->ruleName = $rule->name;
        $auth->add($updateOwnUser);

        $updatePost = $auth->createPermission('dashboard/update');
        // "updateOwnPost" will be used from "updatePost"
        $auth->addChild($updateOwnUser, $updatePost);

        $admin = $auth->createPermission('admin');
        // allow "author" to update their own posts
        $auth->addChild($admin, $updateOwnUser);
        
        // add the "viewOwnuser" permission and associate the rule with it.
        $viewOwnUser = $auth->createPermission('viewOwnUser');
        $viewOwnUser->description = 'View own User';
        $viewOwnUser->ruleName = $rule->name;
        $auth->add($viewOwnUser);

        $viewPost = $auth->createPermission('dashboard/view');
        // "updateOwnPost" will be used from "updatePost"
        $auth->addChild($viewOwnUser, $viewPost);
        $auth->addChild($admin, $viewOwnUser);
    }

    /**
    * assign role
    * @author Praveen Kumar <praveen@makeubig.com>
    */
    protected function assignRole()
    {
        $auth = Yii::$app->authManager;
        
        $superadmin = $auth->createRole('mubadmin');
        $auth->assign($superadmin, 1);
        $admin = $auth->createRole('admin');
        $auth->assign($admin, 2);
        $subadmin = $auth->createRole('subadmin');
        $user = \app\models\User::find()->all();
        foreach ($user as $key => $value) 
        {
            if($value->id > 2)
            {
                $auth->assign($subadmin, $value->id);
            }
        }
    }
}