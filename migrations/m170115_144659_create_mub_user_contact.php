<?php

namespace app\migrations;
use app\commands\Migration;

class m170115_144659_create_mub_user_contact extends Migration
{
    public function getTableName()
    {
        return 'mub_user_contact';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'city' =>['city','id']
        ];
    }

    public function safeUp()
    {
        parent::safeUp();
        $mubUser = new \app\models\MubUser();
        $allUsers = $mubUser::find()->all();
        foreach ($allUsers as $user) 
        {
            $mubUserContacts = new \app\models\MubUserContact();
            $mubUserContacts->mub_user_id = $user->id;
            $mubUserContacts->email = 'admin@mub.com';
            $mubUserContacts->city = 2;
            $mubUserContacts->pin_code= "1100089";
            $mubUserContacts->mobile = "0987654321";
            $mubUserContacts->landline = '1234567890';
            $mubUserContacts->address = "House No:, Street, City";
            if($mubUserContacts->save())
            {
                echo 'created user admin Contact \n';
            }
            else
            {
                p($mubUserContacts->getErrors());
            }
        }
    }

    public function getKeyFields()
    {
        return [
            'mub_user_id'  =>  'mub_user_id',
            'city' => 'city',
            'mobile' => 'mobile',
            'landline' => 'landline'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->defaultValue(NULL),
            'email' => $this->string(50)->notNull(),
            'city' => $this->integer(11)->notNull(),
            'pin_code' => $this->string(100)->notNull(),
            'mobile' => $this->string(100)->notNull(),
            'landline' => $this->string(100)->notNull(),
            'work_phone' => $this->string(100)->notNull()->defaultValue('NA'),
            'address' => $this->string(255)->notNull(),
            'street_address' => $this->string(255)->notNull(),
            'lat' => $this->string(100)->notNull()->defaultValue('NA'),
            'long' => $this->string(100)->notNull()->defaultValue('NA'),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
