<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_090822_create_sub_folder extends Migration
{
    public function getTableName()
    {
        return 'sub_folder';
    }
    public function getForeignKeyFields()
    {
        return [
          'folder_name' => ['folder', 'id'],
      ];
    }

    public function getKeyFields()
    {
        return [
            'sub_folder_name' => 'sub_folder_name',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'folder_name' => $this->string()->defaultValue(NULL),
            'sub_folder_name' => $this->string()->defaultValue(NULL),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
