<?php

namespace app\modules\MubAdmin\controllers;

use Yii;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\City;
use app\models\MubUser;
use yz\shoppingcart\ShoppingCart;
use app\models\HomesListing;
use app\components\Model;
use yii\helpers\ArrayHelper;

class DashboardController extends MubController
{
	public function getProcessModel()
	{

	}

	public function getPrimaryModel()
	{

	}

	public function getDataProviders()
	{
		return [];
	}

    
	public function actionGetCity() {
        if (Yii::$app->request->isAjax) {
            $stateId = Yii::$app->request->post('stateId');
            // $data = [];
            $result = City::find()->where(['state_id' => $stateId])->all();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'result' => $result
            ];
        } else {
            return false;
        }
    }

    public function actionSetAttribute()
    {
        if (Yii::$app->request->isAjax) 
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $params = \Yii::$app->request->getBodyParams();
            $model = new $params['model']();
            $dataModel = $model::findOne($params['id']);
            if($params['model'] == '\\app\\models\\User')
            {
                $mubUser = MubUser::findOne($params['id']);
                $dataModel = $model::find()->where(['id' => $mubUser->user_id,'del_status' => '0'])->one();
            }
            $dataModel->{$params['attribute']} = $params['value'];
            $response['message'] = 'Data Updated successfully';
            if(!$dataModel->save(false))
            {
                $key = array_keys($dataModel->getErrors());
                $response['message'] = $dataModel->getErrors()[$key];
            }
            return $response;
        } 
        else 
        {
            return false;
        }

    }

    public function actionFolder()
    {
        return $this->render('folder');
    }

     public function actionCfolder()
    {
        return $this->render('cfolder');
    }

     public function actionSubfolder()
    {
        return $this->render('Subfolder');
    }

     public function actionSubcfolder()
    {
        return $this->render('subcfolder');
    }

     public function actionForm()
    {
        return $this->render('form');
    }

     public function actionFunction()
    {
        return $this->render('function');
    }

}