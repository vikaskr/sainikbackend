<?php

    function openZip($file_to_open) {
    global $target;
     
    $zip = new ZipArchive();
    $x = $zip->open($file_to_open);
    if($x === true) {
        $zip->extractTo($target);
        $zip->close();
        $zip_created = true;
        echo '<script language="javascript">';
        echo 'alert("File Added")';
        echo '</script>';
         
        unlink($file_to_open);
    } else {
        var_dump($x);
    }
}
 
if(isset($_GET["subfolder"])) {
$subfolder = $_GET["subfolder"]; 
$foldername = $_GET["foldername"]; 
$langfolder = $_GET['language'];
}

if(isset($_POST["subfolder"])) {
$subfolder = $_POST["subfolder"]; 
$foldername = $_POST["foldername"]; 
$langfolder = $_POST['language'];
}

if(isset($_FILES['fupload'])) {
    $filename = $_FILES['fupload']['name'];
    $source = $_FILES['fupload']['tmp_name'];
    $type = $_FILES['fupload']['type']; 
     
    $name = explode('.', $filename); 
    $target = $langfolder .'/'. $foldername .'/'. $subfolder .'/'. $name[0];  
     
    $accepted_types = array('application/zip', 
                                'application/x-zip-compressed', 
                                'multipart/x-zip', 
                                'application/s-compressed');
 
    foreach($accepted_types as $mime_type) {
        if($mime_type == $type) {
            $okay = true;
            break;
        } 
    }
       
  //Safari and Chrome don't register zip mime types. Something better could be used here.
    $okay = strtolower($name[1]) == 'zip' ? true: false;
 
    if(!$okay) {
          die("Please choose a zip file, dummy!");       
    }
     
    mkdir($target, 0777,true);
    $saved_file_location = $target . $filename;
     
    if(move_uploaded_file($source, $saved_file_location)) {
        openZip($saved_file_location);
    } else {
        die("There was a problem. Sorry!");
    }
     
// // This last part is for example only. It can be deleted. 
//     $scan = scandir($target . $name[0]);
     
//     print '<ul>';
//     for ($i = 0; $i<count($scan); $i++) {
//         if(strlen($scan[$i]) >= 3) {
//             $check_for_html_doc = strpos($scan[$i], 'html');
//             $check_for_php = strpos($scan[$i], 'php');
             
//             if($check_for_html_doc === false && $check_for_php === false) {
//                 echo '<li>' . $scan[$i] . '</li>';
//             } else {
//                 echo '<li><a href="' . $target . $name[0] . '/' . $scan[$i] . '">' . $scan[$i] . '</a></li>';
//             }
//         }
//     }
//     print '</ul>';
}
 
?>
 
  
<html>
  <head>
    <title>How to Upload and Open Zip Files With PHP</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
  <body>
    <div id="container">
    <h1>Upload Sainik Magazine Zip File</h1><br />
    <form action="/mub-admin/dashboard/form" method="post" enctype="multipart/form-data">
        <input type="file" name="fupload" /><br />
         <input type="hidden" name="subfolder" value="<?= $subfolder;?>">
         <input type="hidden" name="language" value="<?= $langfolder;?>">
         <input type="hidden" name="foldername" value="<?= $foldername;?>">
        <input type="hidden" name="_csrf" value="ek-wI4U4wBjUMhFl9Hh2JJwL1ydCS0UeC7YJFE07gF9PBMNH6kiyXZ9AaQHZOgBTxW6ycjMEBix9125yDmPMDg==">
        <input type="submit" value="Upload & Extract Zip File" class="btn btn-success" />
    </form>
 
    </div><!--end container-->
  </body>
</html>